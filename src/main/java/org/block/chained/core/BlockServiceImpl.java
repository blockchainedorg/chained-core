/*
 * Copyright (c) 2018 , Joshua T. Pyle and the block-chained contributors.
 */
package org.block.chained.core;

import org.apache.commons.lang3.StringUtils;
import org.block.chained.core.exception.InvalidBlockException;
import org.block.chained.core.model.BlockData;
import org.block.chained.core.model.Block;
import org.block.chained.core.plugin.HashGeneratorPlugin;

import java.util.Objects;

/**
 * TODO Javadoc Everything
 */
public final class BlockServiceImpl<D extends BlockData> implements BlockService<D> {

    private final HashGeneratorPlugin<D> hashGeneratorPlugin;

    public BlockServiceImpl(HashGeneratorPlugin<D> hashGeneratorPlugin) {
        this.hashGeneratorPlugin = hashGeneratorPlugin;
    }


    @Override
    public Block<D> createBlock(String previousHash, D blockData) {
        Block<D> block = new Block<>();
        block.setData(blockData);
        block.setPreviousHash(previousHash);
        block.setHash(calculateHash(block));
        return block;
    }

    @Override
    public String calculateHash(Block<D> block) {
        if(block == null){
            throw new InvalidBlockException("block cannot be null");
        }
        if(block.getPreviousHash() == null){
            throw new InvalidBlockException("block must have a previousHash");
        }
        if(block.getData() == null || StringUtils.isEmpty(block.getData().stringForHash())){
            throw new InvalidBlockException("Block data cannot be null or empty");
        }
        return hashGeneratorPlugin.calculateHash(block);
    }

    @Override
    public Boolean validate(Block<D> block, String expectedHash) {
        if(expectedHash == null){
            throw new RuntimeException("expectedHash cannot be null");
        }
        String hash = calculateHash(block);
        return Objects.equals(hash, expectedHash);
    }
}
