/*
 * Copyright (c) 2018 , Joshua T. Pyle and the block-chained contributors.
 */
package org.block.chained.core;

import org.block.chained.core.model.BlockData;
import org.block.chained.core.model.Block;
import org.block.chained.core.model.BlockPage;

public interface ChainService<D extends BlockData> {

    Block<D> getGenesisBlock();

    Block<D> getBlock(String hash);

    Block<D> addBlock(Block<D> block);

    Block<D> getLastBlock();

    BlockPage<D> getBlocks(String indexHash, int size);

    boolean validateChain();

    long length();

    // TODO : Find Blocks by Searching Data (need api for searching data 'criteria' ?)

}
