/*
 * Copyright (c) 2018 , Joshua T. Pyle and the block-chained contributors.
 */
package org.block.chained.core;

import com.rits.cloning.Cloner;
import org.apache.commons.lang3.StringUtils;
import org.block.chained.core.exception.InvalidBlockException;
import org.block.chained.core.model.BlockData;
import org.block.chained.core.model.Block;
import org.block.chained.core.model.BlockPage;
import org.block.chained.core.plugin.PersistencePlugin;

import java.util.Objects;
import java.util.concurrent.locks.StampedLock;

/**
 * TODO Javadoc Everything
 */
public final class ChainServiceImpl<D extends BlockData> implements ChainService<D> {

    private final BlockService<D> blockService;
    private final PersistencePlugin<D> persistencePlugin;

    private final Block<D> genesisBlock;

    private Block<D> lastBlock;
    private final StampedLock lastLock = new StampedLock();

    private final Cloner cloner = new Cloner();

    public ChainServiceImpl(BlockService<D> blockService, PersistencePlugin<D> persistencePlugin) {
        this.blockService = blockService;
        this.persistencePlugin = persistencePlugin;

        genesisBlock = persistencePlugin.getGenesisBlock();
        lastBlock = persistencePlugin.getLastBlock();
    }

    @Override
    public Block<D> getGenesisBlock() {
        return genesisBlock;
    }

    @Override
    public Block<D> getLastBlock() {
        if (lastBlock == null){
            long stamp = lastLock.writeLock();
            try {
                this.lastBlock = persistencePlugin.getLastBlock();
            }finally {
                lastLock.unlockWrite(stamp);
            }
        }
        long stamp = lastLock.readLock();
        try {
            return cloneBlock(lastBlock);
        }finally {
            lastLock.unlockRead(stamp);
        }
    }

    private Block<D> cloneBlock(Block<D> block){
        return cloner.deepClone(block);
    }

    @Override
    public Block<D> getBlock(String hash) {
        return persistencePlugin.getBlock(hash);
    }

    @Override
    public Block<D> addBlock(Block<D> block) {
        long stamp = lastLock.writeLock();
        try {
            validateNewBlock(block);
            Block<D> newBlock = cloneBlock(block);
            newBlock.setPreviousHash(lastBlock.getHash());
            newBlock.setHash(blockService.calculateHash(newBlock));
            Block<D> persistedBlock = persistencePlugin.save(newBlock);
            this.lastBlock = persistedBlock;
            return persistedBlock;
        }finally {
            lastLock.unlockWrite(stamp);
        }
    }

    private void validateNewBlock(Block<D> block) {
        if(block.getData() == null || StringUtils.isEmpty(block.getData().stringForHash())){
            throw new InvalidBlockException("Block data cannot be null or empty");
        }
    }

    @Override
    public BlockPage<D> getBlocks(String indexHash, int size) {
        return persistencePlugin.getBlocks(indexHash, size);
    }

    @Override
    public boolean validateChain() {
        Block<D> currentBlock = persistencePlugin.getBlock(lastBlock.getPreviousHash());
        if(currentBlock == null){
            // TODO : Log that the never got past the "last" block
            // TODO : Consider destroying the chain and getting a new copy of the chain
            return false;
        }
        // TODO Consider using paged / segmented get instead of individual...
        while (!currentBlock.equals(genesisBlock)){
            String currentHash = blockService.calculateHash(currentBlock);
            if(!Objects.equals(currentBlock.getHash(), currentHash)){
                // TODO : Log that the current hash did not match
                return false;
            }
            currentBlock = persistencePlugin.getBlock(currentBlock.getPreviousHash());
            if(currentBlock == null){
                // TODO : Log that the genesis block was never hit and that the chain is invalid
                // TODO : Consider destroying the chain and getting a new copy of the chain
                return false;
            }
        }
        return true;
    }

    @Override
    public long length() {
        long length = 0;

        Block<D> currentBlock;
        String previousHash = lastBlock.getPreviousHash();
        do{
            // TODO Consider using paged / segmented get instead of individual...
            currentBlock = persistencePlugin.getBlock(previousHash);
            if (currentBlock == null){
                throw new RuntimeException("Chain Ended Prematurely");
            }
            previousHash = currentBlock.getPreviousHash();
            length = length + 1;
            if(length == Long.MAX_VALUE){
                throw new RuntimeException("Chain Never Ending ????");
            }
        }while (!currentBlock.equals(genesisBlock));
        return length + 1;
    }
}
