/*
 * Copyright (c) 2018 , Joshua T. Pyle and the block-chained contributors.
 */
package org.block.chained.core.model;

import java.util.Objects;
import java.util.StringJoiner;

/**
 * Represents the block stored on the blockchain.
 *
 * @param <D> The data being stored in the block it must extend the {@link BlockData} interface
 */
public final class Block<D extends BlockData> {
    private String hash;
    private D data;
    private String previousHash;

    public Block() {
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public D getData() {
        return data;
    }

    public void setData(D data) {
        this.data = data;
    }

    public String getPreviousHash() {
        return previousHash;
    }

    public void setPreviousHash(String previousHash) {
        this.previousHash = previousHash;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Block<?> block = (Block<?>) o;
        return Objects.equals(hash, block.hash) &&
                Objects.equals(data, block.data) &&
                Objects.equals(previousHash, block.previousHash);
    }

    @Override
    public int hashCode() {
        return Objects.hash(hash, data, previousHash);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Block.class.getSimpleName() + "[", "]")
                .add("hash='" + hash + "'")
                .add("data=" + data)
                .add("previousHash='" + previousHash + "'")
                .toString();
    }
}
