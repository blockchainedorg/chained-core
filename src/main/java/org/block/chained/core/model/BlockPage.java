/*
 * Copyright (c) 2018 , Joshua T. Pyle and the block-chained contributors.
 */
package org.block.chained.core.model;

import java.util.List;

/**
 * Represents a segment or 'Page' of the blockchain
 *
 * @param <D> The data being stored in the block it must extend the {@link BlockData} interface
 */
public interface BlockPage<D extends BlockData> {
    String getIndex();

    int getSize();

    List<Block<D>> getContent();
}
