/*
 * Copyright (c) 2018 , Joshua T. Pyle and the block-chained contributors.
 */
package org.block.chained.core.model;

import java.util.List;
import java.util.Objects;
import java.util.StringJoiner;

/**
 * An implementation of the {@link BlockPage} interface.
 *
 * @param <D> The data being stored in the block it must extend the {@link BlockData} interface
 */
public final class BlockPageImpl<D extends BlockData> implements BlockPage<D> {

    private String index;

    private int size;

    private List<Block<D>> content;

    @Override
    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    @Override
    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    @Override
    public List<Block<D>> getContent() {
        return content;
    }

    public void setContent(List<Block<D>> content) {
        this.content = content;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BlockPageImpl<?> blockPageImpl = (BlockPageImpl<?>) o;
        return size == blockPageImpl.size &&
                Objects.equals(index, blockPageImpl.index) &&
                Objects.equals(content, blockPageImpl.content);
    }

    @Override
    public int hashCode() {
        return Objects.hash(index, size, content);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", BlockPageImpl.class.getSimpleName() + "[", "]")
                .add("index='" + index + "'")
                .add("size=" + size)
                .add("content=" + content)
                .toString();
    }
}