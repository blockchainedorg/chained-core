/*
 * Copyright (c) 2018 , Joshua T. Pyle and the block-chained contributors.
 */
package org.block.chained.core.plugin;

import org.block.chained.core.model.BlockData;
import org.block.chained.core.model.Block;

/**
 * Implement this interface to use a consistent hashing algorithm.
 *
 * @param <D> The data being stored in the block it must extend the {@link BlockData} interface
 */
public interface HashGeneratorPlugin<D extends BlockData> {
    /**
     * Calculate a hash for hte given Block
     *
     * @param block te block to calculate the hash for
     * @return the calculated hash
     */
    String calculateHash(Block<D> block);
}
