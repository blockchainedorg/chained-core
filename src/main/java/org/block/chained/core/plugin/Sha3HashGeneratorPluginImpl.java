/*
 * Copyright (c) 2018 , Joshua T. Pyle and the block-chained contributors.
 */

package org.block.chained.core.plugin;

import org.block.chained.core.model.Block;
import org.block.chained.core.model.BlockData;
import org.bouncycastle.jcajce.provider.digest.SHA3;
import org.bouncycastle.util.encoders.Hex;

import java.lang.reflect.InvocationTargetException;
import java.nio.charset.Charset;

public class Sha3HashGeneratorPluginImpl<D extends BlockData> implements HashGeneratorPlugin<D> {

    private final Class<? extends SHA3.DigestSHA3> digestType;

    public Sha3HashGeneratorPluginImpl(Class<? extends SHA3.DigestSHA3> digestType) {
        this.digestType = digestType;
    }

    @Override
    public String calculateHash(Block<D> block) {
        // TODO Throw an exception when block is null
        String previousHash = block.getPreviousHash();
        // TODO Throw and exception when missing previous hash?
        // TODO Throw an exception when data is null
        String dataString = block.getData().stringForHash();
        // TODO Throw an exception when dataString is null or empty
        String input = String.format("%s:%s", previousHash, dataString);
        try {
            SHA3.DigestSHA3 sha3 = digestType.getDeclaredConstructor().newInstance();
            sha3.update(input.getBytes(Charset.forName("UTF-8")));
            return Hex.toHexString(sha3.digest());
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            // TODO Log the exception
            throw new RuntimeException(String.format("%s could not be instantiated: %s", digestType.getName(), e.getMessage()));
        }

    }
}
